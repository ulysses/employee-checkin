import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import ResponseModal from './responseModal';
import FeedbackModal from './feedbackModal';
import PageTemplate from './smallComponents/pageTemplate';
// import messages from '../assets/messages.png';
// import rate from '../assets/smallRate.png';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import moment from 'moment';
    class Queue extends React.Component {

      state = {
       
        commentModalShow: false,
        feedbackModalShow: false,
    
        message: 'I am comming out.',
        feedback: 'No feedback yet',
        ith: ''
      };

// state = {scanned: [], sent: []}

//       componentDidMount() {
        
//         this.setState({scanned: scanned})
//         //runs to automatically respond
//         //console.log('auto token:', this.props.reducer.autoToken);
//         //console.log('token 1 :', this.props.reducer.token1);
//         //this.sendPushNotification();
//         //this.props.reducer.autoToken && this.sendPushNotification();  
        
//         this.setState({sent: sent})
          
//       }

getData=(val)=>{
  // do not forget to bind getData in constructor
  // this.setState({message: val})
  this.setState({message: val})
}

getData1=(val)=>{
  // do not forget to bind getData in constructor
  // this.setState({message: val})
  this.setState({feedback: val})
}

handleMessage =(i) => {
  

  //alert('Choose one of our ready made messages or enter a custom message below. You can set your perferred messages in your profile page.')
  this.setState({ commentModalShow: true, ith: i })
  
}

handleFeedback =(i) => {
  

  //alert('Choose one of our ready made messages or enter a custom message below. You can set your perferred messages in your profile page.')
  this.setState({ feedbackModalShow: true,ith: i })
  
}
   
componentDidMount(){ 
 //if(this.props.reducer.autoToken) { console.log('sending auto message to: ', this.props.reducer.autoToken); this.sendPushNotification(token = this.props.reducer.autoToken, title = '', body = 'this is an automatic message', data = 'this is an automatic message'); this.props.clearAutoToken(null); } 
 //console.log('running on page landing')
}

closeCommentModal = () => {
  
  this.setState({ commentModalShow: false})
  this.props.responseSent(this.state.message)
  this.sendPushNotification(token = this.props.reducer.token1[this.state.ith], title = '', body = this.state.message, data = this.state.message)
  // `i will be back at ${this.props.reducer.messageTimes.dblTime}`

}

closeFeedbackModal = () => {
  
  this.setState({ feedbackModalShow: false})

  firebase.database().ref('Feedbacks/' + this.props.reducer.token[this.state.ith].replace('[','').replace(']','') + '/' + moment() + '/').set({
    feedback: this.state.feedback,
    
    
   
})

}


      sendPushNotification(token, title, body, data) {
        //this.props.clearAutoToken(null); //clear the token
        console.log(data)
        return fetch('https://exp.host/--/api/v2/push/send', {
          body: JSON.stringify({
            to: token,
            title: title,
            body: body,
            data: {data}
          }),
          headers: {
            'Content-Type': 'application/json',
          },
          method: 'POST',
        });
      }

      



    render() {
      let scanned = Array.from(Array(this.props.reducer.scanned).keys())
      let sent = Array.from(Array(this.props.reducer.scan).keys())
      
      const { navigate } = this.props.navigation;
      const navigateBack=()=>{
        navigate('Home')
      }

      // let chatBtn =



      return(
       
       <React.Fragment>
     <PageTemplate title={'Queue'} navigate={navigateBack} />
     
        <View style={{ height:40, borderBottomWidth:2,borderTopWidth:2, borderBottomColor: '#ededed', borderTopColor: '#ededed',flexDirection: 'row',justifyContent:'center',alignItems:'center' }}>
          <Text style={styles.sectionHeader}>Requests Received</Text>
          </View>
{     scanned && 
        scanned.map((i) => {
         
          return <View key={i} style={{height:90, backgroundColor: '#ededed',justifyContent:'space-between'}}>
            <View style={{flexDirection: 'row',justifyContent:'space-between',alignItems:'center' }}><Text style={styles.item}>Request: {this.props.reducer.messageReceived[i]}</Text>
          <TouchableOpacity 
          onPress={()=>{this.handleMessage(i)}}
          style={{
              borderWidth:1,
      marginRight: 10,
              width:45,
              height:45,
              backgroundColor:'#fff',
              borderRadius:50
            }}>
              <Ionicons  name="ios-chatbubbles" size={38} />
        </TouchableOpacity></View><Text style={styles.item}>Response: {this.props.reducer.responseSent[i]}</Text></View>                            
        }) 
      }
       <View style={{ height:40, borderBottomWidth:2,borderTopWidth:2, borderBottomColor: '#ededed', borderTopColor: '#ededed',flexDirection: 'row',justifyContent:'center',alignItems:'center' }}>
         <Text style={styles.sectionHeader}>Requests Sent</Text>
         </View>

{     sent && 
  sent.map((i) => {
    return <View key={i} style={{height:90, backgroundColor: '#ededed',}}>
      <View style={{flexDirection: 'row',justifyContent:'space-between',alignItems:'center' }}><Text style={styles.item}>Request: {this.props.reducer.messageSent[i]}</Text>
    <TouchableOpacity 
    disabled={this.state.feedback === 'Positive' || this.state.feedback === 'Negative' ? true : false }
    onPress={()=>{this.handleFeedback(i)}}
    style={{
        borderWidth:1,
marginRight: 10,
        width:45,
        height:45,
        backgroundColor:'#fff',
        borderRadius:50
      }}>
        <Ionicons  name = {this.state.feedback === 'Positive' || this.state.feedback === 'Negative' ? 'ios-checkmark-circle' : "ios-happy"} size={38} color={this.state.feedback === 'Positive' || this.state.feedback === 'Negative' ? 'grey' : 'black' }/>
  </TouchableOpacity>
  </View><Text style={styles.item}>Response: {this.props.reducer.responseReceived[i]}</Text></View>                            
  }) 
}

{this.state.commentModalShow && ( <ResponseModal 
                                               sendData={this.getData}
                                              modalVisible={this.state.commentModalShow} 
                                              closeCommentModal={this.closeCommentModal} 
                                              navigate={this.props.navigate}  />)}

{this.state.feedbackModalShow && ( <FeedbackModal 
                                               sendData={this.getData1}
                                              modalVisible={this.state.feedbackModalShow} 
                                              closeFeedbackModal={this.closeFeedbackModal} 
                                              navigate={this.props.navigate}  />)}
 

       </React.Fragment>
      );
    }
  }


  const styles = StyleSheet.create({

    btn : {
      borderWidth:1,
      borderColor:'rgba(0,0,0,0.2)',
      alignItems:'center',
      justifyContent:'center',
      width:35,
      height:35,
      backgroundColor:'#fff',
      borderRadius:50,
      marginTop: 150
    },
    
    btnWrapper: {
      position: 'absolute',
      flexDirection: 'row',
      flex: 2,
      justifyContent:'space-between',
      padding:15 
    },
  
    sliderWrapper: {
      position: 'absolute',
      marginTop: '150%',
      flexDirection: 'row'
    },
  
    textBox: {
      position: 'absolute',
      flexDirection: 'row',
      flex: 1.5,
      marginLeft: '25%',
      marginTop: '30%', 
    },
  
    text: {
      position: "absolute",
      color: "white",
      textAlign: 'center',
      fontSize: 30
    },
    
    textInput: {
      position: 'absolute',
      flexDirection: 'row',
      flex: 1.5,
      marginLeft: '25%',
      marginTop: '150%', 
    },
  
    container: {
      flex: 1,
      paddingTop: 22
     },
     item: {
       padding: 10,
       fontSize: 12,
       height: 46,
       fontWeight: 'bold'
     },
     sectionHeader: {
      paddingTop: 2,
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 2,
      fontSize: 18,
      fontWeight: 'bold'
    }
    })
  
    const mapStateToProps = (state) => {
   
      const { reducer } = state
      return { reducer }
    };
  
    const mapDispachToProps = dispatch => {
      return {
        clearAutoToken: (x) => dispatch({ type: "CLEAR_AUTO_MESSAGE", value: x}),
        responseSent: (x) => dispatch({ type: "RESPONSE_SENT", value: x})
        
      };
    };


    
    export default connect(mapStateToProps, mapDispachToProps
      )(Queue)

  /**
   * 
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'grey'}}>
          <Text> Queue page </Text>
        </View>
   * 





          <View style={styles.btnWrapper}>
            <TouchableOpacity 
              style={{
                borderWidth:1,
                //borderColor:'rgba(0,0,0,0.2)',
                alignItems:'center',
                justifyContent:'center',
                width:35,
                height:35,
                backgroundColor:'#fff',
                borderRadius:50
              }}>
                <Image source={messages}/>
            </TouchableOpacity>
          </View>


          <View>
              <Text>Your Placed Requests</Text>
              <Text>Request 1 . . . time: 16:48 . . . time since sent: 4:32</Text>
          </View>

          <View><Text>Requests for you to Move </Text></View>            
            
            
            <Text> Queue page </Text>

           
          



   */