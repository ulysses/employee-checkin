import React, { Component } from 'react';
import {
  View,
  Dimensions,
  Button,
  Share,
} from 'react-native';
//import { Constants, FileSystem } from 'expo';
import * as FileSystem from 'expo-file-system';
import * as Sharing from 'expo-sharing'; // Import the library
import ViewShot from "react-native-view-shot";
import QRCode from 'react-native-qrcode-svg';

export default class App extends Component {
  static navigationOptions = {
    title: 'Pdf Screen',
  };

  state = {
    pdfUrl: '',qr:null
  };

   makeDowload() {
     FileSystem.downloadAsync(
      'http://gahp.net/wp-content/uploads/2017/09/sample.pdf',
      FileSystem.documentDirectory + 'small.pdf'
    )
      .then(({ uri }) => {
        console.log('Finished downloading to ', uri);
        this.setState({ pdfUrl : uri})
      })
      .catch(error => {
        console.error(error);
      });

  }


  onShare = async () => {
    console.log('attempting to share pdf', this.state.pdfUrl);
    console.log('attempting to share qr', this.state.qr);
    try {
        //const uri = <QRCode value={'here is my code'}/>
      const result = await Share.share({
        message:'my file',
        url: this.state.qr//FileSystem.documentDirectory + 'small.pdf'
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };


  shareIt=()=>{

    Sharing.shareAsync(this.state.qr) // And share your file !
  }


  onCapture = uri => {
    console.log("do something with ", uri);
    this.setState({ qr: uri})
  }

  render() {
    this.state.uri && console.log(this.state.uri);
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          margin: 10,
        }}
      >

        <View style={{marginLeft: '10%', marginBottom: '40%', backgroundColor: 'blue', position:'absolute'}}>
          <ViewShot onCapture={this.onCapture} captureMode="mount" >
          
              <QRCode value={'hello'}/>
          
        </ViewShot>
        </View>

    <View style={{marginBottom: '80%'}}>        
        <Button          
          title="download"
          onPress={() => {
            this.makeDowload();
          }}/>

        <Button          
          title="share"
          onPress={() => {
            this.shareIt();
            //this.onShare();
          }}/>

</View>

      </View>
    );
  }
}