import React, { Component } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import PureChart from 'react-native-pure-chart';

class pie extends Component {
    render() {

        let sampleData = [
            {
              value: 70,
              label: 'progress',
              color: 'orange',
            }, {
              value: 30,
              label: 'Sales',
              color: '#d3d3d3'
            }
        
          ]


        return (
            <View>

                <PureChart data={sampleData} type='pie' />

                {/**create a circle */}
                <View style={styles.circle} />
                <View
                    style={styles.textWrapper}
                >
                    <Text style={styles.text}>450</Text>
                    <Text style={styles.text2}>of 500 pts</Text>
                </View>   

            </View>
        );
    }
}

export default pie;


const styles = StyleSheet.create({
    circle: {
        position: 'absolute',
        marginTop: 35,
        marginLeft: 35,
        borderRadius: 70,
        width: 130,
        height: 130,
        borderWidth: 5,
        borderColor: 'white',
        backgroundColor: 'white'
      },
    textWrapper: {
        position: 'absolute',
        marginTop: 70,
        marginLeft: 70,
        
    },
    text:{
        fontSize: 40,
        fontWeight: 'bold',
        textAlign:'center'
    },
    text2:{
        fontSize: 15,
        textAlign:'center'
    }
  });