import React, {Component} from 'react'
import {
  Alert,
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Button,
  ScrollView,
  Modal,
  ActivityIndicator
} from 'react-native';

import { Ionicons } from '@expo/vector-icons';
// import * as Sharing from 'expo-sharing'; // Import the library
// import * as Contacts from 'expo-contacts';
// import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { composeAsync } from 'expo-mail-composer';
import { isAvailableAsync, sendSMSAsync } from 'expo-sms';
import {
  getContactsAsync,
 
  EMAILS,
  PHONE_NUMBERS,
} from 'expo-contacts';


class InviteScreen extends Component {

    state = {
        inviteVisible: this.props.inviteVisible,
        message: 'None',
        selectedContacts: [],
    }

    // checks to makes sure phone number is clean
    validatePhone(data){
        let x = data.replace('+', '').replace(')','').replace('(','').replace(' ','').replace('-','');
        //x = data.replace('/[()]/g','');
        return x
      }
    
    //fetch contact information from phone
      fetchContacts = async () => {
          console.log('fetching contacts')
            let { status } = await Permissions.getAsync(Permissions.CONTACTS);
            if (status !== Permissions.PermissionStatus.GRANTED) {
              // TODO: Handle permissions denied.
              console.log('permissions have been denied')
              await Permissions.askAsync(Permissions.CONTACTS);
            } else {
              console.log('permission granted')
              
              //pull data from contacts          
              //const { data } = await Contacts.getContactsAsync({
              //fields: [Contacts.Fields.Emails],
              //});
    
              const { data } = await getContactsAsync({
                fields: [EMAILS, PHONE_NUMBERS],
              });
    
              //if data exists, map over it and prepare for state load
              if (data.length > 0) {
                let contact = data.reduce((acc, {
                  id,
                  name,
                  phoneNumbers,
                  emails
                  }) => {
                    return [...acc, {
                      'id': name ? name:'hey',
                      'name': name ? name : 'hey',
                      'phone': phoneNumbers ?  this.validatePhone(phoneNumbers[0]['number']) : null,
                      'email': emails ?  emails[0].email : null
                      }];
                  }, []);
    
                //sort data
                contact = contact.sort(function (a, b) {
                    var textA = a.name.toUpperCase();
                    var textB = b.name.toUpperCase();
                    return textA.localeCompare(textB);
                });
    
                //set to state
                this.setState({ contacts: contact.slice(0,15).filter(person => !(/\d/.test(person.name))) })
                //filter(c => c.name === 'Lausto Fake' || c.name === 'Nicholas Lopez The Marine')
              }
            }
        };
    
        //add selected contact to the state
        addSelectedContact =(data) => {
          var joined = this.state.selectedContacts.concat(data);
          this.setState({ selectedContacts : joined })
        }
    
        //initiate invites
        onInvitePress = async () => {
          let didShare = false;
          const message = `Send app link to your friends: [link goes here/promo code]`;
          const emails = this.state.selectedContacts
            .filter(c => c.email != null)
            .map(c => c.email);
    
          const phoneNumbers = this.state.selectedContacts
            .filter(c => c.phone != null)
            .map(c => c.phone);
    
          this.setState({ clean:phoneNumbers })
          console.log('clean numbers: ', phoneNumbers);
    
        //share emails
         if (emails.length > 0) {
          try {
            const result = await composeAsync({
              recipients: emails,
              subject: 'Hello friend',
              body: message,
              isHtml: false,
            });
            didShare = didShare || result.status === 'sent';
          } catch (ex) {
            Alert.alert(ex.message);
          }
        }
    
        //share phone numbers
        if (phoneNumbers.length > 0 && (await isAvailableAsync())) {
          try {
            const result = await sendSMSAsync(phoneNumbers, message);
            didShare = didShare || result.result === 'sent';
          } catch (ex) {
            Alert.alert(ex.message);
          }
        }
    
        if (didShare) {
          Alert.alert('Thanks for sharing!!');
        }
    
    
    
        };



    render() {

        //render the contact on a map
        const ContactRow = React.memo(
            ({ onPress, name, email, phone, selected }) => {
              return (
                
                <TouchableHighlight onPress={onPress}>
                  <View
                    style={{ padding: 16, flexDirection: 'row', alignItems: 'center' }}>
                       <Text style={{ marginRight: 16 }}>{selected ? '✅' : '⭕️'}</Text>
                      <View style={{ flex: 1 }}>
                        <Text>{name}</Text>
                        {name.length > 0 && (
                          <View>
                          <Text style={{ marginTop: 4, color: '#666' }}>
                            {email}
                          </Text>
                          <Text style={{ marginTop: 4, color: '#666' }}>
                            {phone}
                          </Text>
                          </View>
                        )}
                      </View>
                    </View>
                </TouchableHighlight>
      
                    );
                }
              );
       
        return (
            <React.Fragment>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.inviteVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>

        
        <TouchableOpacity
             onPress={this.props.closeInvite}
        >
            <Ionicons  name="ios-arrow-round-back" size={40} />
        </TouchableOpacity>
        <Text
          style={{
            paddingTop: 28,
            paddingBottom: 8,
            paddingHorizontal: 16,
            fontSize: 22,
        }}>
          Invite friends
        </Text> 

        <ScrollView>       
            {this.props.contacts ? this.props.contacts.map(( contact, index) =>{
              return <ContactRow
                    key={index}
                    onPress={ ()=> this.addSelectedContact({id: contact.name,name: contact.name, email:contact.email,phone:contact.phone})}
                    name={ contact.name } 
                    email={ contact.email }
                    phone={  contact.phone}
                    selected={Boolean(this.state.selectedContacts.find(item => item.name === contact.name && item.phone === contact.phone))}
                    />
            }) : <View style={{marginTop: '50%'}}><ActivityIndicator size="large" color="#0000ff" /></View>
          }
        </ScrollView>

        <View>
          <Button  title={`Invite Selected Contacts (${this.state.selectedContacts.length})`} onPress={()=>this.onInvitePress()}/>
        </View>


        </Modal>
            </React.Fragment>
        );
    }
}

export default InviteScreen;