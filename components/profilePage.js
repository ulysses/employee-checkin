
import React from 'react';
import { StyleSheet, Text, View,
   TouchableOpacity,Alert, ActivityIndicator, AsyncStorage } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import {  ListItem } from 'react-native-elements'

// import GenerateQR from './generateQR';
import PageTemplate from './smallComponents/pageTemplate';
// import pic from '../assets/bigRate.png';
import * as firebase from 'firebase';

import Firebase from '../constants/config';

import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
// import * as Expo from 'expo'
import { connect } from 'react-redux';
// import Constants from 'expo-constants';
// import * as Location from 'expo-location';
import moment from "moment";
import { ScrollView } from 'react-native-gesture-handler';

const STORAGE_KEY = '@save_enableauto';
const DBLTIME_KEY = '@save_dbltime';

class Profile extends React.Component {

state = {fname: null, lname: null, phone: null, email: null, data: null, autoToken:null, location: null,
  errorMessage: null, enableAuto:null, dblTime: null}

uid = firebase.auth().currentUser.uid;

//local storage calls are triggered when listener receives notification
_retrieveData = async (notification) => {
  try {
    let enableAuto = await AsyncStorage.getItem(STORAGE_KEY);
    let dblTime = await AsyncStorage.getItem(DBLTIME_KEY);
    
    //dblTime = eavl(dblTime);
    enableAuto = eval(enableAuto);
    
    //test if auto notifications are enabled
    if (enableAuto) {
      console.log('local data: ',enableAuto);
      console.log('local time: ',dblTime)
        //-------------------------------------------------------------
        //if times exist use the time, if not add 5 minutes and push 
        if(dblTime){
         
        //console.log('time has been set')
          this.setState({ dblTime });

          this.sendPushNotification(token = notification.data.data[0],
            notification.data.data[1], 
            title = 'I am sorry...', 
            body = `I am sorry to block you, I will be returning at ${dblTime}`, 
            data = 'dblTime');


        } else {

        // We have data!!
          console.log('auto msgs enabled');
          this.setState({ enableAuto });

        //test store automated messaging data, if null meaning messages are not set we will ad 5 minutes to the current time
          let defaultAuto = moment().add(5, "minutes").format("YYYY-MM-DD HH:mm:00");

        //fire notifications automatically back to the incoming token 
          this.sendPushNotification(token = notification.data.data[0],
            notification.data.data[1], 
            title = 'I am sorry...', 
            body = `I am sorry to block you, I will be returning at ${defaultAuto}`, 
            data = defaultAuto);

        }
        //------------------------------------------------------------


      }

    } catch (error) {
      alert('failed to load previous settings.')
    // Error retrieving data
  }

};

//get permissions for location
// componentWillMount() {
//   if (Platform.OS === 'android' && !Constants.isDevice) {
//     this.setState({
//       errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
//     });
//   } else {
//     this._getLocationAsync();
//   }
// }
// //run location grab 
// _getLocationAsync = async () => {
//   let { status } = await Permissions.askAsync(Permissions.LOCATION);
//   if (status !== 'granted') {
//     this.setState({
//       errorMessage: 'Permission to access location was denied',
//     });
//   }

//   let location = await Location.getCurrentPositionAsync({});
//   location =  await JSON.stringify(location);
  
//   this.setState({ location });
// };


  componentDidMount() {

 
    //console.log(this.uid);
    // if (firebase.auth().currentUser) {
    //   userId = firebase.auth().currentUser.uid;
    //   if (userId) {
    //       firebase.database()..ref('UsersList/' + userId + '/info/').set({
    //           email: firebase.auth().currentUser.email,
              
    //           uid: userId,
             
    //       })
    //   }
    // }
    
    firebase.auth().currentUser.emailVerified && this.readUserData();
    this._notificationSubscription = this.registerForPushNotificationsAsync();

      // Notifications.addListener(this.listen)

      this._notificationSubscription = Notifications.addListener(
        this._handleNotification
      );

      if (firebase.auth().currentUser.emailVerified == false){
        Alert.alert(
          'ALERT!',
          'Please verify your email first.',
          [
            
            {text: 'OK'}
          ],
          {cancelable: false}
        );
        firebase.auth().signOut()
        }
      
  }

  componentWillUnmount() {
    console.log('unmounted')
    this._notificationSubscription && this._notificationSubscription.remove();
  
    
  }








  _handleNotification = (notification) => {
    
    if (Array.isArray(notification.data.data)){
      
      console.log(notification.data.data)
      console.log('gggggggg')
      this.props.Scanned()
      this.props.setToken1(notification.data.data[0],notification.data.data[1]);
      this.props.genAutoToken(notification.data.data[0],notification.data.data[1]);
      
      //call local storage to check whether messaging is enable, we pass the notification
      this._retrieveData(notification);
      
    } else {
      console.log('not')
      console.log(notification.data.data)
      this.props.responseReceived(notification.data.data)
    } 
    
    this.props.navigation.navigate('Queue')
    
    

    //run auto token
    // console.log('recieving token:',notification.data.data)
    
    
  };






  // listen = (notification) => {
  //   this.props.Scanned()
  //   this.props.navigation.navigate('Queue')
    
  //   this.props.setToken1(notification.data.data)
    
  //   // this.props.setToken1(JSON.stringify(notification.data))
  // }


  registerForPushNotificationsAsync = async()=>{
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
  
    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
  
    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
      return;
    }
  
    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();

    // console.log(token)
    // // firebase.database().ref('users/' + userId).push({token: token })
    // var updates = {}
    // updates['/expoToken'] = token

    // firebase.database().ref('users/' + userId).update(updates)


    this.props.setToken(token);
    
  }


  // sendPushNotification(token = this.state.token, title = 'test', body = 'test') {
  //   return fetch('https://exp.host/--/api/v2/push/send', {
  //     body: JSON.stringify({
  //       to: token,
  //       title: title,
  //       body: body,
  //       data: { message: `${title} - ${body}` },
  //     }),
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     method: 'POST',
  //   });
  // }

  sendPushNotification(token, title, body, data) {
    //this.props.clearAutoToken(null); //clear the token
    console.log(data)
    return fetch('https://exp.host/--/api/v2/push/send', {
      body: JSON.stringify({
        to: token,
        title: title,
        body: body,
        data: {data}
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    });
  }


    signOut = () => {
      firebase.auth().signOut()
        .catch(error => console.log(error))
    }

    handleSignout = () => {
      console.log('signing out')
      Alert.alert(
        'Are you sure you want to sign out?',
        'Confirm Signing Out',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed!')},
          {text: 'OK', onPress: this.signOut},
        ]
        )
    
    }

    readUserData() {
      Firebase.database().ref('UsersList/'+this.uid + '/info').once('value', snapshot => {
      console.log('user data:',snapshot.val());    
      let data = snapshot.val()//[this.uid]

          //console.log('our data: ',data);
          this.setState({ fname: data.fname,
                          lname: data.lname,
                          phone: data.phone,
                          email: data.email,
                          data: data});
          //this.calculateProgress(data);
          this.state.data && this.props.setData(this.state.data);
          

      })
      
    }

    sendLocation(uid,timestamp,lat,lon,type){
      Firebase.database().ref('UsersList/' + uid + '/locations/').push({
        uid,
        timestamp,
        lat,
        lon,
        type
      }).then((data)=>{
        //success callback
        console.log('data ' , data)
      }).catch((error)=>{
        //error callback
        console.log('error ' , error)
      })
    }


    render() {
      const { navigate } = this.props.navigation;
      navigateBack=()=>{
        navigate('Queue')
      }

      //handle unix datetime
      handleDate =(data)=>{
        return new Date(data * 1000).toISOString().slice(0, 19).replace('T', ' ');
      }

      //this.state.data && console.log('from state ',this.state.data)
      //this.props.reducer.autoToken && console.log('from store', this.props.reducer.autoToken)
      
      //run location grab 
      //let text = 'Waiting to grab from profile..';
      //  if (this.state.errorMessage) {
      //    text = this.state.errorMessage;
      //  } else if (this.state.location) {
      //    text = this.state.location;
      //    text = eval( '(' + '[' + text + ']' + ')' )
      //  }


        //text[0].timestamp && console.log(this.uid, this.state.timestamp, text[0].coords.latitude, text[0].coords.longitude );
        //text[0].timestamp && this.sendLocation(this.uid, this.state.timestamp, text[0].coords.latitude, text[0].coords.longitude, 'profile');
      

      return(


        <View style={{flex:1}}>
          {/** </View>{this.props.reducer.data ? */}
          {/* {this.props.reducer.data ?
          //show profile */}

          <React.Fragment>
           
         
          <PageTemplate title={'Profile'} />
           
           {/*<Image source={pic} />*/}
           {/**  Frst Section */}
 
           <View style={{ 
                   //backgroundColor:'blue'
                   }}>
                   <Text style={styles.section}>Account Information</Text>
                   
           </View>

           {/** ACCOUNT INFORMATION NAME, PHONE ETC SLOT */}
           
           {this.props.reducer.data ? <TouchableOpacity
                         onPress={()=>{navigate('EditAccount')}}
                         style={{position: 'absolute',
                         marginTop:'33%',
                         flex:1,
                         flexDirection: 'row',
                         flexWrap: 'wrap'}}>
           

               
               <View style={{paddingTop:50,
                              padding:15
                           }}
                             >
                 <Text style ={styles.accText}>{this.props.reducer.data && this.props.reducer.data.fname + ' ' + this.props.reducer.data.lname}</Text>
                 <Text style ={styles.accText}>{this.props.reducer.data && this.props.reducer.data.phone}</Text>
                 <Text style ={styles.accText}>{this.props.reducer.data && this.props.reducer.data.email}</Text>
 
               </View>
             
          
           </TouchableOpacity> :  <View style={{marginTop:'5%',alignItems:'center'}}><Text>loading</Text><ActivityIndicator size="large" color="#0000ff" /></View>}
           
           {/**add line section */}
           
           <View style={{
                     //backgroundColor:'yellow', 
                     marginTop:'33%'
                     }}>
                   <Text style={styles.section}>Settings</Text>
          </View>
         <ScrollView>
             <ListItem
               onPress={()=>{navigate('QR')}}
               title={`My Qr Code`}
               subtitle={`Print out your QR code to receive messages`}
               leftElement={<Ionicons  name="ios-qr-scanner" size={32} />}
               rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
             />
 
             <ListItem
               onPress={()=>{navigate('Messaging')}}
               title={`My Messages`}
               subtitle={`Edit your messages and receive points for the more information you add`}
               leftElement={<Ionicons  name="ios-chatboxes" size={32} />}
               rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
             />
 
             <ListItem
               onPress={()=>{navigate('Rewards')}}
               title={`My Rewards`}
               subtitle={`Check the status of your points to redeem rewards`}
               leftElement={<Ionicons  name="ios-gift" size={32} />}
               rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
             />
             {/**
             <ListItem
               onPress={()=>{navigate('Privacy')}}
               title={`Privacy`}
               subtitle={`Adjust your privacy settings`}
               leftElement={<Ionicons  name="ios-settings" size={32} />}
               rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
             />
              */}
             <ListItem
               onPress={this.handleSignout}
               title={`Sign Out`}
               subtitle={`Press to sign out`}
               leftElement={<Ionicons  name="ios-close-circle" size={32} />}
               rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
             />

</ScrollView>
           
              
 
 
           
               
 
           {/** <GenerateQR /> */}
         </React.Fragment>

          {/* // //show loader
          // :<View style={styles.loader}>
          //   <ActivityIndicator size="large" color="#0000ff" />
          //   <Text>Loadingg...</Text>
          // </View>

          // } */}
        </View>







     
      );
    }
  }

  const styles = StyleSheet.create({

    option : {
      //position: 'absolute',
      marginLeft:'5%',
      alignSelf: 'stretch',
      width: '100%',
      height: '15%',
      //flexWrap: 'wrap',
      justifyContent:'space-between',
      //padding:20 
    },
    section : {
      fontSize:20,
      marginLeft:'3%',
      marginTop: '5%'
    },
    loader: {
      marginTop: '60%',
      alignItems: 'center'
    },
    accText:{
      fontSize: 15
    }

  })

  const mapStateToProps = (state) => {
    
    const { reducer } = state
    return { reducer }
  };
  
  const mapDispachToProps = dispatch => {
    return {
      setToken: (x) => dispatch({ type: "CLOSE_MODAL_13", value: x}),
      setToken1: (x,y) => dispatch({ type: "CLOSE_MODAL_14", value: x, value1: y}),
      genAutoToken: (z) => dispatch({ type: "PUSH_AUTO_MESSAGE", value: z}),
      setData: (y) => dispatch({ type: "GET_USER_DATA", value: y}),
      Scanned: () => dispatch({ type: "GOT_SCANNED", one: 1}),
      responseReceived: (x) => dispatch({ type: "RESPONSE_RECEIVED", value: x})
    };
  };


  
  export default connect(mapStateToProps,
    mapDispachToProps
    )(Profile)


  /**
   * 
   * 
   * 

                  <View style={styles.option}>
                    <Ionicons  name="ios-gift" size={32} />
                    <TouchableHighlight>
                      <Text>Rewards</Text>
                    </TouchableHighlight>
                  </View>



                  
   * 
   */