import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight,
   View, Alert, TextInput, Button, StyleSheet} from 'react-native';

import PageTemplate from './smallComponents/pageTemplate';
import { connect } from 'react-redux';
import { RadioGroup } from 'react-native-btr';
class ScanModal extends Component {
  state = {
    modalVisible: this.props.modalVisible,
    mes: 'You double-parked me. Please, leave ASAP.',
    data: [
      {
        label: 'You double-parked me. Please, leave ASAP.',
        value: 'You double-parked me. Please, leave ASAP.',
        checked: true
      },
      {
        label: 'You blocked my drive-way. Please, leave ASAP.',
        value: 'You blocked my drive-way. Please, leave ASAP.'
      },
      {
        label: 'You are blocking my car. Please, leave ASAP.',
        value: 'You are blocking my car. Please, leave ASAP.',
      },


    ]
  };

  onPress = (data) => {
    this.setState({data })

    let selectedButton = this.state.data.find(e => e.checked == true);
    
    selectedButton = selectedButton ? selectedButton.value : this.state.data[0].label;
    this.setState({mes:selectedButton})
    this.props.sendData(selectedButton)
 

  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    
    return (
     
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View >
            <PageTemplate title={'Custom Message'}/>

            
            <View style={{backgroundColor:'white',flex:1, padding:10}} />
                    
            <View style={{padding: 20}}>
              {/* <Text>Enter a custom message below. This will overide any of your automatic settings in your profile. If you leave this empty we will use an automatic message or one of your preset message:</Text> */}
            <View style={{backgroundColor:'white',flex:1, padding:10}} />
              {/* <TextInput
                style={{ 
                    borderRadius: 10,
                    height: 40,
                    maxHeight: 80,
                    width: 350, 
                    borderColor: 'gray', 
                    borderWidth: 1, 
                    marginLeft: 10}}
                    onChangeText={mesg => {this.setState({mes: mesg}), this.props.sendData(mesg)}}
                    value={this.state.mes}
              /> */}
              <RadioGroup radioButtons={this.state.data} onPress={this.onPress} />
               <View style={{backgroundColor:'white',flex:1, padding:10}} />
              <View style={styles.btnWrapper}>
                <Button
                  title={"Send"}
                  // onPress= {this.props.closeCommentModal, this.props.Mes(this.state.mes)}
                  onPress={()=> {this.props.closeCommentModal(), this.props.Mes(this.state.mes)}}
                   
                  >
                </Button>
              </View>
              
              
            </View>
          </View>
        </Modal>

        <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}>
          <Text>Show Modal</Text>
        </TouchableHighlight>
      </View>
    );
  }
}



const mapStateToProps = (state) => {
   
  const { reducer } = state
  return { reducer }
};

const mapDispachToProps = dispatch => {
  return {
    Mes: (x) => dispatch({ type: "MES", value: x})
   
  };
};

export default connect(mapStateToProps,mapDispachToProps
  )(ScanModal)


const styles = StyleSheet.create({

  btn : {
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    alignItems:'center',
    justifyContent:'center',
    width:35,
    height:35,
    backgroundColor:'#fff',
    borderRadius:50,
    marginTop: 150
  },
  
  btnWrapper: {
    width: '40%',
    margin: 20
  },
})



