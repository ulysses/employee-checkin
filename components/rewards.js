//https://stackoverflow.com/questions/44603362/setting-a-timer-for-a-long-period-of-time-i-e-multiple-minutes
//yellow box issue we can ignore

import React, { Component } from 'react';
import { StyleSheet, Text, View, 
    Animated, ScrollView, Alert,
     Dimensions, TouchableOpacity, Modal } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
// import { List, ListItem } from 'react-native-elements';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import { connect } from 'react-redux';
// import * as moment from "moment";
// import * as Sharing from 'expo-sharing'; // Import the library
// import * as Contacts from 'expo-contacts';
// import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
// import { composeAsync } from 'expo-mail-composer';
// import { isAvailableAsync, sendSMSAsync } from 'expo-sms';
import {
  getContactsAsync,
  Contact,
  EMAILS,
  PHONE_NUMBERS,
} from 'expo-contacts';

// import GenerateQR from './generateQR';
import Gift from './smallComponents/gift';
// import Amazon from '../assets/amazon.png';
// import Starbux from '../assets/starbux.png';
// import Pie from './smallComponents/pie';
import InviteScreen from './smallComponents/inviteScreen';

//import GiftCards from '../constants/rewardsConfig';
import * as gifts from '../constants/rewardsConfig';

//initalize firebae db 
import Firebase from '../constants/config';
import * as firebase from 'firebase';
import ignoreWarnings from 'react-native-ignore-warnings';

//let db = Firebase.database();
//let rewardsRef = db.ref('/rewards');

ignoreWarnings('Setting a timer');
class rewards extends Component {

    state = {
        inviteVisible:false,
        contacts: [],
        goal:50000,
        rewards: null,
        progress:null,
        progressWithOnComplete: 0,
        progressCustomized: 0,
        rewardsData : {
            goal:20000,
            rewards:null,

        }
        //uid:'znqiuJIFH7QYlFDyWd1kLIHnBkG3'
    };

    uid = firebase.auth().currentUser.uid;
    rewards = this.props.reducer.data.rewards;

    readUserData() {
        Firebase.database().ref('UsersList/'+this.uid+'/info').once('value', snapshot => {
            
            if (snapshot.val() === null) {
                this.setState({ rewards: this.rewards });
                this.calculateProgress(this.rewards);
            } else {
                let data = snapshot.val()
                //console.log('our data: ',data);
                this.setState({ rewards: data.rewards });
                this.calculateProgress(data.rewards);

            } 
        })
        
    }

    calculateProgress (data) {
        let progress =  Math.round(data/this.state.goal * 100);
        this.setState({ progress: progress });
        console.log(progress);
    }


    alterRewards (value) {
        //console.log('received data ', value);
        //console.log(this.state.rewards)
        let data = this.state.rewards - value;
        //console.log('ready to send', data)        
        this.pushReward(data);
    }

    pushReward (data) {
        console.log('sending data', data, 'to', this.uid);
        
        //send to main info file
        Firebase.database().ref('UsersList/' + this.uid + '/info/rewards').set(
            data    
        );

        //also send to reward element
        this.rewardsNode(this.uid,data);

        //this.logReward(this.uid,'1',data);
        this.updateRewards();
    }

    rewardsNode (uid,data) {
        let rewards = data;
        Firebase.database().ref('UsersList/' + uid + '/rewards').push({
            uid,
            rewards    
        }).then((data)=>{
            //success callback
            console.log('data ' , data)
          }).catch((error)=>{
            //error callback
            console.log('error ' , error)
          })
    }

    logReward (id,rewards,type) {
        console.log(id)
        let time =new Date(); //moment().format("YYYY-MM-DD HH:mm:00");
        Firebase.database().ref('Rewards/' + id).set({
          type,
          rewards,
          time
      }).then((data)=>{
          //success callback
          console.log('data ' , data)
      }).catch((error)=>{
          //error callback
          console.log('error ' , error)
      })
      }

    updateRewards () {
        this.readUserData();
    }

    chooseReward (data) {
        //console.log('pressed-------------------', data)

        Alert.alert(
             this.props.reducer.data.fname + ` are you sure you want to buy this reward, it will cost you ${data} points?`,
            '',
            [
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed!')},
              {text: 'OK', onPress:()=>this.alterRewards(data)},
            ]
            )
    }


    componentDidMount () {
        this.readUserData();
        //this.setState({ rewards: this.rewards});
        //console.log('store passed:', this.props.reducer.data.uid)
    }

    increase = (key, value) => {
        this.setState({
          [key]: this.state[key] + value,
        });
    }

    openInvite =() => {

        // checks to makes sure phone number is clean
        const validatePhone=(data)=>{
            let x = data.replace('+', '').replace(')','').replace('(','').replace(' ','').replace('-','');
            //x = data.replace('/[()]/g','');
            return x
        }

        //fetch contact information from phone
        const fetchContacts = async () => {
            console.log('fetching contacts')
              let { status } = await Permissions.getAsync(Permissions.CONTACTS);
              if (status !== Permissions.PermissionStatus.GRANTED) {
                // TODO: Handle permissions denied.
                console.log('permissions have been denied')
                await Permissions.askAsync(Permissions.CONTACTS);
              } else {
                console.log('permission granted')
                
                //pull data from contacts          
                //const { data } = await Contacts.getContactsAsync({
                //fields: [Contacts.Fields.Emails],
                //});
      
                const { data } = await getContactsAsync({
                  fields: [EMAILS, PHONE_NUMBERS],
                });
      
                //if data exists, map over it and prepare for state load
                if (data.length > 0) {
                  let contact = data.reduce((acc, {
                    id,
                    name,
                    phoneNumbers,
                    emails
                    }) => {
                      return [...acc, {
                        'id': name ? name:'hey',
                        'name': name ? name : 'hey',
                        'phone': phoneNumbers ?  validatePhone(phoneNumbers[0]['number']) : null,
                        'email': emails ?  emails[0].email : null
                        }];
                    }, []);
      
                  //sort data
                  contact = contact.sort(function (a, b) {
                      var textA = a.name.toUpperCase();
                      var textB = b.name.toUpperCase();
                      return textA.localeCompare(textB);
                  });
      
                  //set to state
                  this.setState({ contacts: contact.slice(0,30).filter(person => !(/\d/.test(person.name))) })
                  //filter(c => c.name === 'Lausto Fake' || c.name === 'Nicholas Lopez The Marine')
                }
              }
          };
        fetchContacts();
        this.setState({ inviteVisible: true });
    }

    closeInvite =() => {
        this.setState({ inviteVisible: false });
    }
    
    
    

    
    render() {

        //set up loop for component
        

        //console.log(firebase.auth().currentUser.email);
        //console.log(firebase.auth().currentUser.uid);
        //console.log('my obj: ',gifts.giftCards[0])

        
        let cards = null;
        if(this.state.rewards) {
            cards = (
                <View>
                    {
                        gifts.giftCards.map( (card, i) => {
                            return <View key={i}>

                            <Gift key={i}
                            value={card.value}
                            toDo={() => this.chooseReward(card.value)}
                            pic={card.pic}
                            title={`$ ${card.id} Amazon Gift Card`}
                            caption={''}
                        />
                        </View>
                        })
                    }
                </View>
            )
        }

        
        

       const barWidth = Dimensions.get('screen').width - 130;

        return (
            <ScrollView style={{marginTop: 30}}>
                <View style={styles.container}>
                <Text
                    style={{fontSize:25, textAlign:'center', fontWeight:'bold'}}
                >{this.props.reducer.data.fname && `Welcome to your Rewards ${this.props.reducer.data.fname}!`}</Text>
                <Text
                    style={{fontSize:15, textAlign:'center'}}
                >{`Your Points: ${this.state.rewards ? this.state.rewards.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0}`}</Text>

                <Text>{`of ${this.state.goal}`}</Text>

                <Text>Next available reward: </Text>

                {/**PROGRESS BAR */}
                {/** <Pie rewards={this.state.rewards} />*/}
                
                <View style={styles.containerBar}>
                    <View style={{alignItems: 'center'}}>
                        <Text>
                            Progress to Next Reward
                        </Text>
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <ProgressBarAnimated
                            width={barWidth}
                            height={30}
                            value={this.state.progress}
                            backgroundColorOnComplete="#6CC644"
                        />
                    </View>
                <Text>
                    {`${this.state.progress}% Progress`}
                </Text>

                </View>

                {/** Referral Position */}
                <View>
                    <Text>WANT TO EARN MORE?</Text>
                    <Text>Refer a friend to receive more points</Text>
                    <TouchableOpacity onPress={this.openInvite}>
                        <Ionicons  name="ios-share-alt" size={32} />
                    </TouchableOpacity>
                </View>

                {/**OPEN INVITE OPTIONS */}
                {this.state.inviteVisible && ( <InviteScreen
                                                    contacts={this.state.contacts}
                                                    closeInvite={this.closeInvite}
                                                    inviteVisible={this.state.inviteVisible}
                                                />)}


                {/**GIFT CARD OPTIONS */}
                <Text>MoveIt Cash Offers</Text>
                <Text>Your MoveIt points can be used to redeem the following deals.</Text>
                {cards}
                </View>
             </ScrollView>    
              
            
        );
    }
}

const mapStateToProps = (state) => {
    console.log('rewards:', state)
    const { reducer } = state
    return { reducer }
  };

  const mapDispachToProps = dispatch => {
    return {
      pullUser: () => dispatch({ type: "GET_USER_DATA", value: false}),
    };
  };
  
  export default connect(mapStateToProps,
   mapDispachToProps
    )(rewards)
  

//export default rewards;

const styles = StyleSheet.create({
    container: {
      //backgroundColor: 'yellow',
      //flex: 1,
      //flexWrap: 'wrap',
      alignItems: 'center',
      justifyContent:'space-between',
      padding:15 
    },
    containerBar: {
        //flex: 1,
        //flexDirection: 'Column',
        //justifyContent: 'center',
        //alignItems: 'center',
        //paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
        //padding: 8,
        //width: '90%'
          alignSelf: 'center',
          padding: 12,
      },
      progressBar: {
        flexDirection: 'row',
        //height: '100%',
        width: '100%',
        backgroundColor: 'white',
        borderColor: '#000',
        borderWidth: 2,
        borderRadius: 5
      },
      label: {
        color: '#999',
        fontSize: 14,
        fontWeight: '500',
        marginBottom: 10,
      },
  })


  /**
   * 
   * let cards = null;
        if(this.state.rewards) {
            cards = (
                <View>
                    {
                        gifts.giftCards.map( (card, index) => {
                            return <Gift
                            value={gifts.giftCards['amz']['1']}
                            toDo={() => this.chooseReward(gifts.giftCards['amz']['1'])}
                            pic={gifts.cardPics.amz}
                            title={'$10'}
                            caption={'here is a gift card'}
                        />
                        })
                    }
                </View>
            )
        }
   * 
   */