import React, {Component} from 'react';
import {Text, TouchableOpacity,
   View, Button, Alert, Image, Modal} from 'react-native';
   import { Ionicons } from '@expo/vector-icons';
import QRCode from 'react-native-qrcode-svg';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import Firebase from '../constants/config';
import PageTemplate from './smallComponents/pageTemplate';
import Share from './smallComponents/share';
// //import Save from './smallComponents/save';

// import * as FileSystem from 'expo-file-system';
import * as Sharing from 'expo-sharing'; // Import the library
import ViewShot from "react-native-view-shot";

class generateQR extends Component {

    state = {
        id: null,
        showCode: true, //turned to true to nulify button
        pdfUrl: '',
        qr:null,
        showShare:true,
        showModal:false,
        modalColor: 'rgba(0,0,0,0.7)'        
    }

    handleCode=()=>{
        this.setState({ showCode: true, showShare: true })
    //         firebase.database().ref('Users/').once('value', function (snapshot) {
    //     console.log(snapshot.val())
    // });
    console.log(this.props.reducer.token)
    }

    shareIt=()=>{
        Sharing.shareAsync(this.state.qr) // And share your file !
      }
    
    
      onCapture = uri => {
        console.log("do something with ", uri);
        this.setState({ qr: uri})
      }

      help () {
        this.setState({showModal:true})
      }

      closeModal(){
        this.setState({showModal:false})
      }




    render() {
        
        const { navigate } = this.props.navigation;
        navigateBack=()=>{
          navigate('Profile')
        }

        return (
            <View style={{flex:1}}> 
                    <View style={{padding:10, marginTop:'5%'}}>
                        <Text style={{fontSize:17}}>Your personalized QRCode is below. Click share code to email the code to yourself and print out.</Text>    
                    </View>

                    {/** QR CODE BELOW */}
                    {
                        this.state.showCode ?
                        <View style={{marginTop:'10%'}}>
                            <ViewShot onCapture={this.onCapture} captureMode="mount" >
                                <View style={{backgroundColor:'white', padding: 10, alignItems:'center'}}>    
                                    <QRCode value={this.props.reducer.token} size={200}/>   
                                </View>
                            </ViewShot>
                        </View> 
                        :<View style={{margin:110,width:"40%"}}>
                            <Button title={'Show my Code'} onPress={this.handleCode} /> 
                         </View>
                    }

                    {/**SHARING BUTTON */}
                    {this.state.showShare && 
                            <TouchableOpacity
                                    onPress={() => {
                                    this.shareIt();
                                    }}>
                                <View style={{alignItems:'center', marginTop: '10%'}}>
                                    <Ionicons name="ios-share" size={50} />
                                    <Text style={{fontSize:17}}>Share Code</Text>
                                </View>
                            </TouchableOpacity> }
                    
                    {/**HELP BUTTON */}
                    <TouchableOpacity
                                    onPress={() => {
                                    this.help();
                                    }}>
                                <View style={{alignItems:'center', marginTop: '10%'}}>
                                    <Ionicons name="ios-help-circle" size={50} />
                                    <Text style={{fontSize:17}}>Help</Text>
                                </View>
                            </TouchableOpacity>

                            {
                               this.state.showModal && <Modal transparent={true}
                               visible={this.state.showModal}
                               onRequestClose={this.closeModal}>
                          <View style={{
                                  flex: 1,
                                  flexDirection: 'column',
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                  backgroundColor:this.state.modalColor}}>
                            <View style={{
                                    width: 325,
                                    height: 365,
                                    backgroundColor:'white',
                                    borderWidth: 1,
                                    borderColor:'black',
                                    borderRadius:5}}>
                                      
                              <Text style={{textAlign:'center', padding:10, fontSize:17}}>Once you print your QR code place it on your car, preferably on your windshield so others can scan and send you messages.</Text>
                              
                              <View style={{alignItems:'center', marginBottom:-30}}>
                                <Image source ={require('../assets/instruction.png')} />
                              </View>

                              <View style={{width:'30%', marginLeft:'35%'}}>
                                <Button title={'okay'} onPress={()=>this.closeModal()} />
                              </View>

                            </View>
                          </View>
                        </Modal> 
                            }
                                    
            </View>
        );
    }
}

const mapStateToProps = (state) => {
   
    const { reducer } = state
    return { reducer }
  };
  
//   const mapDispachToProps = dispatch => {
//     // return {
//     //   setToken: (x) => dispatch({ type: "CLOSE_MODAL_13", value: x})
     
//     // };
//   };
  
  export default connect(mapStateToProps
    )(generateQR)



/**
 *   <View>
                    <QRCode content='https://reactnative.com'/>
                </View>


                 <View style={{margin: 20}}>
                        <TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1 }} secureTextEntry={true}  />
                    
                    </View>



                    
 */
