import * as React from 'react';
import { Text, View, StyleSheet, Button,  Animated, TouchableOpacity, TextInput, Image, Slider } from 'react-native';
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { Camera } from 'expo-camera';


export default class BarcodeScannerExample extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false,
    modalVisible: false,
    data: null,
    zoom: 0
  };

  async componentDidMount() {
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  };

  onValueChange(value) {
    this.setState({ zoom: value });
  }

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      
      <View style={styles.container}>

        <Camera
          onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}
          zoom={this.state.zoom}
        />
        
             {scanned && (
          <Button title={'Tap to Scan Again'} onPress={() => this.setState({ scanned: false })} />
        )}
      </View>

    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true, modalVisible: true, zoom:0 });
    this.props.navigate('Queue')
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };
}
