import React from 'react';
import {Text} from 'react-native';
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack';


import { Provider } from 'react-redux';
import { createStore } from 'redux';

import ourReducer from './store/reducer';

import { Ionicons } from '@expo/vector-icons';
//main tabs
import Scanner from './components/mainPage';
import Queue from './components/queuePage';
import Info from './components/infoPage';
import Profile from './components/profilePage';
import Login from './components/Login'
import SignUp from './components/Signup'
import Load from './components/Loading'

//Profile Page Imports
import GenerateQR from './components/generateQR';
import EditAccount from './components/editAccount';
import Rewards from './components/rewards';
import Messaging from './components/messaging';


const store = createStore(ourReducer);
//firebase.initializeApp(ApiKeys.FirebaseConfig)
export default class App extends React.Component {


  render() {


    
    return (
      <Provider store={ store }>


        <AppContainer />

      
        </Provider>
    );
  }
}



 //Navigate profiles
 const ProfileNavigator = createStackNavigator({
  //  bottomTabNavigator: 
   
   Profile: { screen: Profile,
         navigationOptions: {
          headerShown: false
      }
  },
  
   QR: { screen: GenerateQR,
          navigationOptions: ({ navigation }) => {
      return {
        headerTitle: <Text style={{ fontSize: 35, color: 'white'}}>My QR Code</Text>,
        headerStyle:{backgroundColor:'black', height: 100},
        headerTintColor: '#ffffff',
        //headerLeft: (<HeaderBackButton onPress={_ => navigation.goBack()}/>)
      }
    } 
  
  },
    Messaging: { screen: Messaging, 
     navigationOptions: ({ navigation }) => {
       return {
         headerTitle: <Text style={{ fontSize: 35, color: 'white'}}>Messaging</Text>,
         headerStyle:{backgroundColor:'black', height: 100},
         headerTintColor: '#ffffff',
         //headerLeft: (<HeaderBackButton onPress={_ => navigation.goBack()}/>)
       }
     }
   },
   Rewards: { screen: Rewards,

    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: <Text style={{ fontSize: 35, color: 'white'}}>Rewards</Text>,
        headerStyle:{backgroundColor:'black', height: 100},
        headerTintColor: '#ffffff',
        //headerLeft: (<HeaderBackButton onPress={_ => navigation.goBack()}/>)
      }
    }
  
  },
   //Privacy: { screen: Privacy },
   EditAccount: { screen: EditAccount,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: <Text style={{ fontSize: 35, color: 'white'}}>Account Information</Text>,
        headerStyle:{backgroundColor:'black', height: 100},
        headerTintColor: '#ffffff',
        //headerLeft: (<HeaderBackButton onPress={_ => navigation.goBack()}/>)
      }
    }
  
  }
 }

 );


const bottomTabNavigator = createBottomTabNavigator(
  {
    Scanner: {
      screen: Scanner,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Ionicons  name="ios-qr-scanner" size={25} color={tintColor}/>
          // <Icon name="qrcode" size={25} color={tintColor} />
        )
      }
    },
    Queue: {
      screen: Queue,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          // <Icon name="comments" size={25} color={tintColor} />
          <Ionicons  name="ios-chatbubbles" size={25} color={tintColor}/>
        )
      }
    },
    Info: {
      screen: Info,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          // <Icon name="search" size={25} color={tintColor} />
          <Ionicons  name="ios-information-circle" size={25} color={tintColor}/>
        )
      }
    },
    Profile: {screen: ProfileNavigator,navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        // <Icon name="user" size={25} color={tintColor} />
        <Ionicons  name="ios-person" size={25} color={tintColor}/>
      )
    }}

  },
  {
    initialRouteName: 'Profile',
    tabBarOptions: {
      activeTintColor: '#eb6e3d'
    }
  }
);




const RootSwitch = createSwitchNavigator({ 
                  Load, 
                  SignUp, 
                  Login, 
                  
                  bottomTabNavigator
                  });

const AppContainer = createAppContainer(RootSwitch);



// //Stack Navigators----------------------------------------------------------------------
// //Each one of these allows us to navigate within the tabs created on the top

//Create a navigation options that will present a header for navigation use
/** 

 */
// //Navigate the comment modal
// const QRNavigator = createStackNavigator({
//   Home: { screen: Home},
//   //CommentModal: { screen: CommentModal, navigationOptions }
// });

// //Navigate profiles
// const ProfileNavigator = createStackNavigator({
//   Profile: { screen: Profile},
//   QR: { screen: GenerateQR, navigationOptions },
//   Rewards: { screen: Rewards, navigationOptions },
//   Privacy: { screen: Privacy, navigationOptions },
//   EditAccount: { screen: EditAccount, navigationOptions }
// });

// 


/**
 *       headerLeft: (
        <TouchableWithoutFeedback 
            style={{margin:20}}
            onPress={() => navigation.goBack()} >
            <Text style={{color: "white"}}>go back</Text>
        </TouchableWithoutFeedback>
      )  
 * 
 */


// //couch all navigation across app
// const AppNavigator = createSwitchNavigator({
//   tabs: bottomTabNavigator,
//   profile: ProfileNavigator
// })

// const AppContainer = createAppContainer(AppNavigator);